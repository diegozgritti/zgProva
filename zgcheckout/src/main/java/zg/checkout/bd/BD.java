package zg.checkout.bd;

import zg.checkout.principal.*;

import java.util.HashMap;
import java.util.Map;

public class  BD {
	
	private Map<String, Produto> mydb ;
	
	public BD() {
		this.mydb = new HashMap<String, Produto>();
	}

	public void addIntem(String sku, Integer preco, int quantidadePromo, Integer precoPromo) {
		mydb.put(sku, new Produto(sku, preco, quantidadePromo, precoPromo));
	}
	
	public void removeItem(String sku) {
		mydb.remove(sku);
	}
	
	public Produto getItem(String sku) {
		return mydb.get(sku);
	}

	public Map<String, Produto> getItems() {
		return mydb;
	}

	
	
}
