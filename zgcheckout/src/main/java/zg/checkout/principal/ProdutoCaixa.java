package zg.checkout.principal;

public class ProdutoCaixa {
	private Produto produto;
	private int quantidade;
	private Integer precoTotal;
	private Integer descontoTotal;

	public ProdutoCaixa(Produto produto) {
		this.produto = produto;
		this.quantidade = 0;
		this.precoTotal = 0;
		this.descontoTotal = 0;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void somaUm() {
		this.quantidade++;
		this.alteraPreco();
	}

	public void diminuiUm() {
		if (this.quantidade > 0) {
			this.quantidade--;
			this.alteraPreco();
		}
	}

	public Integer getPrecoTotal() {
		return precoTotal;
	}

	private void setPrecoTotal(Integer precoTotal) {
		this.precoTotal = precoTotal;
	}

	private void alteraPreco() {
		Integer quantidadePromo = this.getProduto().getQuantidadePromo();
		if (this.quantidade > quantidadePromo && quantidadePromo != 0 ) {
			precoTotal = ((this.quantidade % quantidadePromo) * this.produto.getPrecoPromo())
					+ ((this.quantidade / quantidadePromo) * this.produto.getPreco());
			setDescontoTotal(this.quantidade * this.produto.getPreco() - precoTotal);
		} else {
			precoTotal = this.quantidade * this.produto.getPreco();
			setDescontoTotal(0);
		}
	}

	public Integer getDescontoTotal() {
		return descontoTotal;
	}

	private void setDescontoTotal(Integer descontoTotal) {
		this.descontoTotal = descontoTotal;
	}

}
