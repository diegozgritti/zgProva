package zg.checkout.principal;

import zg.checkout.bd.BD;

import java.util.HashMap;
import java.util.Map;

public class Caixa {

	private Map<String, ProdutoCaixa> controleCaixa;
	private Integer precoTotal;
	private Integer descontoTotal;

	public Caixa(BD mydb) {
		controleCaixa= new HashMap<String, ProdutoCaixa>();
		this.precoTotal = 0;
		this.descontoTotal = 0;
		for (Produto produto : mydb.getItems().values()) {
			controleCaixa.put(produto.getSku(), new ProdutoCaixa(produto));
		}
	}

	public void addItem(String sku) {
		Integer precoAntigo = controleCaixa.get(sku).getPrecoTotal();
		Integer descontoAntigo = controleCaixa.get(sku).getDescontoTotal();
		controleCaixa.get(sku).somaUm();
		this.precoTotal = (precoTotal - precoAntigo + controleCaixa.get(sku).getPrecoTotal());
		this.descontoTotal = (descontoTotal - descontoAntigo + controleCaixa.get(sku).getDescontoTotal());
	}

	public void removeItem(String sku) {
		Integer precoAntigo = controleCaixa.get(sku).getPrecoTotal();
		Integer descontoAntigo = controleCaixa.get(sku).getDescontoTotal();
		controleCaixa.get(sku).somaUm();
		this.precoTotal = (precoTotal - precoAntigo + controleCaixa.get(sku).getPrecoTotal());
		this.descontoTotal = (descontoTotal - descontoAntigo + controleCaixa.get(sku).getDescontoTotal());
	}

	public Integer getPrecoTotal() {
		return precoTotal;
	}

	public Integer getDescontoTotal() {
		return descontoTotal;
	}

}
