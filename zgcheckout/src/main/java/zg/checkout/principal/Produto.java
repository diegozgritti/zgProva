package zg.checkout.principal;

public class Produto {

	private String sku;
	private Integer preco;
	private int quantidadePromo;
	private Integer precoPromo;

	public Produto(String sku, Integer preco) {
		this.sku = sku;
		this.preco = preco;
		this.quantidadePromo = 0;
		this.precoPromo = 0;
	}

	public Produto(String sku, Integer preco, int quantidadePromo, Integer precoPromo) {
		this.sku = sku;
		this.preco = preco;
		this.quantidadePromo = quantidadePromo;
		this.precoPromo = precoPromo;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Integer getPreco() {
		return preco;
	}

	public void setPreco(Integer preco) {
		this.preco = preco;
	}

	public int getQuantidadePromo() {
		return quantidadePromo;
	}

	public void setQuantidadePromo(int quantidadePromo) {
		this.quantidadePromo = quantidadePromo;
	}

	public Integer getPrecoPromo() {
		return precoPromo;
	}

	public void setPrecoPromo(Integer precoPromo) {
		this.precoPromo = precoPromo;
	}

}
