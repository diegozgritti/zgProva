package zg.checkout.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import zg.checkout.bd.*;
import zg.checkout.principal.*;

public class TesteCaixa {
	
	private BD db ;
	private Caixa caixa;
	
	@Before
	public void setUp() {
		db = new BD();
		db.addIntem("A", 50, 3, 130);
		db.addIntem("B", 30, 2, 45);
		db.addIntem("C", 20, 3, 40);
		db.addIntem("D", 15, 0, 0);
	}

	@Test
	public void testeTotalUm() throws Exception {
		this.caixa = new Caixa(db);
		caixa.addItem("A");
		assertThat(caixa.getPrecoTotal() == 50);
		assertThat(caixa.getDescontoTotal()== 0);
		caixa.addItem("A");
		assertThat(caixa.getPrecoTotal() == 100);
		assertThat(caixa.getDescontoTotal() == 0);
		caixa.addItem("A");
		assertThat(caixa.getPrecoTotal() == 130);
		assertThat(caixa.getDescontoTotal()  == 20);
		caixa.addItem("A");
		assertThat(caixa.getPrecoTotal() == 180);
		assertThat(caixa.getDescontoTotal()  == 20);
		caixa.addItem("A");
		assertThat(caixa.getPrecoTotal() == 230);
		assertThat(caixa.getDescontoTotal() == 20);
		caixa.addItem("A");
		assertThat(caixa.getPrecoTotal() == 260);
		assertThat(caixa.getDescontoTotal()  == 40);
		caixa.removeItem("A");
		assertThat(caixa.getPrecoTotal() == 230);
		assertThat(caixa.getDescontoTotal()  == 20);
	}
	
	@Test
	public void testeTotalDois() throws Exception {
		this.caixa = new Caixa(db);
		caixa.addItem("D");
		assertThat(caixa.getPrecoTotal() == 15);
		assertThat(caixa.getDescontoTotal() == 0);
		caixa.addItem("A");
		assertThat(caixa.getPrecoTotal() == 65);
		assertThat(caixa.getDescontoTotal() == 0);
		caixa.addItem("B");
		assertThat(caixa.getPrecoTotal() == 95);
		assertThat(caixa.getDescontoTotal() == 0);
		caixa.addItem("A");
		assertThat(caixa.getPrecoTotal() == 145);
		assertThat(caixa.getDescontoTotal() == 0);
		caixa.addItem("B");
		assertThat(caixa.getPrecoTotal() == 160);
		assertThat(caixa.getDescontoTotal() == 15);
		caixa.addItem("A");
		assertThat(caixa.getPrecoTotal() == 190);
		assertThat(caixa.getDescontoTotal() == 35);
		caixa.removeItem("A");
		assertThat(caixa.getPrecoTotal() == 160);
		assertThat(caixa.getDescontoTotal() == 15);
		caixa.removeItem("B");
		assertThat(caixa.getPrecoTotal() == 145);
		assertThat(caixa.getDescontoTotal() == 0);

	}
	
	@Test
	public void testeTotalTres() throws Exception {
		this.caixa = new Caixa(db);
		caixa.addItem("C");
		assertThat(caixa.getPrecoTotal() == 20);
		assertThat(caixa.getDescontoTotal() == 0);
		caixa.addItem("C");
		assertThat(caixa.getPrecoTotal() == 40);
		assertThat(caixa.getDescontoTotal() == 0);
		caixa.addItem("C");
		assertThat(caixa.getPrecoTotal() == 40);
		assertThat(caixa.getDescontoTotal() == 20);
		caixa.addItem("C");
		assertThat(caixa.getPrecoTotal() == 60);
		assertThat(caixa.getDescontoTotal() == 20);
		caixa.removeItem("C");
		assertThat(caixa.getPrecoTotal() == 40);
		assertThat(caixa.getDescontoTotal() == 20);
		caixa.removeItem("C");
		assertThat(caixa.getPrecoTotal() == 40);
		assertThat(caixa.getDescontoTotal() == 0);
	}
	
	@Test
	public void testeTotalQuatro() throws Exception {
		this.caixa = new Caixa(db);
		caixa.addItem("C");
		assertThat(caixa.getPrecoTotal() == 20);
		assertThat(caixa.getDescontoTotal() == 0);
		caixa.addItem("B");
		assertThat(caixa.getPrecoTotal() == 50);
		assertThat(caixa.getDescontoTotal() == 0);
		caixa.addItem("B");
		assertThat(caixa.getPrecoTotal() == 65);
		assertThat(caixa.getDescontoTotal() == 15);
		caixa.removeItem("B");
		assertThat(caixa.getPrecoTotal() == 50);
		assertThat(caixa.getDescontoTotal() == 0);
	}

}
